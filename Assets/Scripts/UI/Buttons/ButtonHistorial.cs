using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TestIshiharaCode;

public class ButtonHistorial : ButtonChangeScene
{
    // Start is called before the first frame update
    protected override void TaskOnClick()
    {
      string path = Application.dataPath + "/StreamingAssets/testResults.json";
        bool filexists1 = ResultSave.IfFileExists(path);
        if (filexists1 == false)
            ResultSave.CreateJsonFile(path);
        base.TaskOnClick();
    }
}
