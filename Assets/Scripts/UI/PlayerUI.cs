using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using TestIshiharaCode;

public class PlayerUI : MonoBehaviour
{
    private GameObject _playerName;
    // Start is called before the first frame update
    void Start()
    {
        //Para conservar el nombre del jugador entre escenas.
        _playerName = GameObject.Find("PlayerName");
        DontDestroyOnLoad(_playerName);
    }

    // Update is called once per frame
    void Update()
    {
        //Constantemente el GameObject PlayerName esta obteniendo el nomnbre que introduce el usuario.
        _playerName.GetComponent<TMP_Text>().text = GameObject.Find("Text").GetComponent<TMP_Text>().text;
    }
}
