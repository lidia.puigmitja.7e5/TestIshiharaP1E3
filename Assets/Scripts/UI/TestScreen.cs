using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using TestIshiharaCode;
using System.IO;


//Classe que gestiona las pantallas del Text
public class TestScreen : MonoBehaviour
{
    [SerializeField] 
    //Array con todas las imagenes del test
    private Sprite[] _testImages;
    //Primer Dropdown
    private TMP_Dropdown _firstDrop;
    //Segundo Dropdown
    private TMP_Dropdown _secondDrop;
    //GameObject usado para ocultar el segundo Dropdown.
    private GameObject _hidesecondDrop;
    //Guarda el valor actual del primer Dropdown
    private string _valueFirstDrop;
    //Guarda el valor actual del segundo Dropdown
    private string _valueSecondDrop;
    //Variable para recorrer la Array de sprites. Comienza en 0. 
    private int _count;
    private string pathQuestion;
    private int questionId;
    private string image;
    private string CorrectValue;
    private string diagnostico;
    private string userFilePath;
    private string username;
    private GameObject _diagnostico;
    void Start()
    {
        _count = 0;
        //Coloca la primera imagen. 
        GameObject.Find("Image").GetComponent<Image>().sprite = _testImages[_count];
        _firstDrop = GameObject.Find("DropdownPrimer").GetComponent<TMP_Dropdown>();
        _secondDrop = GameObject.Find("DropdownSegon").GetComponent<TMP_Dropdown>();
        _hidesecondDrop = GameObject.Find("DropdownSegon");
        username = GameObject.Find("PlayerName").GetComponent<TMP_Text>().text;
         userFilePath = Application.dataPath + "/StreamingAssets/" + username + ".json";
        pathQuestion = Application.dataPath + "/StreamingAssets/TestQuestions.json";
        ResultSave.CreateJsonFile(userFilePath);
    }
    private void Update()
    {
        if(_count<_testImages.Length)
        if (_testImages[_count].name.Contains("-") || _count == 17 || _count == 18)
            _hidesecondDrop.SetActive(true);
        else _hidesecondDrop.SetActive(false);
    }

    //Al pulsar el boton next. 
    public void TaskOnClik()
    {
        _count++;
        QuestionManager.SelectQuestion(_count, pathQuestion);
        questionId = QuestionManager.GetQuestionId();
        image = QuestionManager.GetImage1();
        CorrectValue = QuestionManager.GetIncorrectValue1();
        if (_count < _testImages.Length)
        {
            _valueFirstDrop = _firstDrop.options[_firstDrop.value].text;
            //Value es el indice de la array.
            _firstDrop.value = 0;
            if (_secondDrop.IsActive())
            {
                _valueSecondDrop = _secondDrop.options[_secondDrop.value].text;
                _secondDrop.value = 0;
                string _valueDrop = _valueFirstDrop + "-" + _valueSecondDrop;
                AnswerSave.AddAnswerToJsonFile(questionId, image, _valueDrop, CorrectValue, userFilePath);
            }
            diagnostico = AnswerSave.SelectUserData(userFilePath);

            GameObject.Find("Image").GetComponent<Image>().sprite = _testImages[_count];
        }


        else
        {
            string path = Application.dataPath + "/StreamingAssets/testResults.json";
            bool filexists1 = ResultSave.IfFileExists(path);
            if (filexists1 == false)
                ResultSave.CreateJsonFile(path);
            int nameCounter = ResultSave.SelectCountName(username, path);
            if (nameCounter > 4)
            {
                ResultSave.DeleteHistory(username, path);
            }

            string testDate = ResultSave.GetDateTimeNow();
            ResultSave.AddTextToJsonFile(username, diagnostico, testDate, path);

            File.Delete(userFilePath);
            _diagnostico = GameObject.Find("Diagnostico");
           _diagnostico.GetComponent<TMP_Text>().text = diagnostico;
            DontDestroyOnLoad(_diagnostico);
            SceneManager.LoadScene("Resultat", LoadSceneMode.Single);
        }
    }
}
