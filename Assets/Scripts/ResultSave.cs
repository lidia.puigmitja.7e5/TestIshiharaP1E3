﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace TestIshiharaCode
{
    class ResultSave
    {
        //Funció: Mostrar per consola tot el contingut del fitxer de guardat.
        //Paràmetres: string amb la ruta del fitxer.
        public static void PrintJsonByConsole(string path)
        {
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    User user1 = JsonConvert.DeserializeObject<User>(line);
                    string json = JsonConvert.SerializeObject(user1);
                    Console.WriteLine(json);
                }
            }
        }


        //Funció: Crear un arxiu de tipus json he introduïr contingut.
        //Paràmetres: string amb el nom de l'usuari, int amb la puntuació, string amb el tipus de daltonisme, string amb la ruta del fitxer.
        public static void CreateFiledJsonFile(string userName, string diagnostic, string testDate, string path)
        {
            var user1 = new User
            {
                Name = userName,
                Diagnostic = diagnostic,
                TestDate = testDate
            };

            string json1 = JsonConvert.SerializeObject(user1);

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(json1);
            }
        }


        //Funció: Crear un arxiu de tipus json sense contingut.
        //Paràmetres: string amb la ruta del fitxer.
        public static void CreateJsonFile(string path)
        {
            //StreamWriter sw = new StreamWriter(path);
            using (StreamWriter sw = new StreamWriter(path))
            {
            }
        }


        //Funció: Afegir contingut al fitxer json.
        //Paràmetres: string amb el nom de l'usuari, int amb la puntuació, string amb el tipus de daltonisme, string amb la ruta del fitxer.
        public static void AddTextToJsonFile(string userName, string diagnostic, string testDate, string path)
        {
            var user1 = new User
            {
                Name = userName,
                Diagnostic = diagnostic,
                TestDate = testDate
            };

            string json1 = JsonConvert.SerializeObject(user1);
            File.AppendAllText(path, json1 + "\n");
        }


        //Funció:Seleccionar un usuari pel nom i mostrar les seves dades.
        //Paràmetres: string amb el nom de l'usuari, string amb la ruta del fitxer.
        public static void SelectUserData(string userName, string path)
        {
            string userName1 = "", diagnostic1 = "", testDate1 = "";

            List<User> user1 = new List<User>();
            List<User> user2 = new List<User>();
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    User user = JsonConvert.DeserializeObject<User>(line);
                    user1.Add(user);
                }
            }

            //Guarda les dades de l'usuari seleccionat en variables.
            foreach (User user in user1)
            {
                if (user.Name == userName)
                {
                    userName1 = user.Name;
                    diagnostic1 = user.Diagnostic;
                    testDate1 = user.TestDate;
                    //user2.Add(user);
                }

            }
        }


        //Funció: Actualitzar les dades de l'usuari.
        //Paràmetres: string amb el nom de l'usuari, int amb la puntuació, string amb el tipus de daltonisme, string amb la ruta del fitxer.
        public static void UpdateUserData(string userName, string diagnostic, string testDate, string path)
        {

            List<User> user1 = new List<User>();
            List<User> user2 = new List<User>();
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    User user = JsonConvert.DeserializeObject<User>(line);
                    user1.Add(user);
                }
            }


            foreach (User user in user1)
            {
                if (user.Name == userName)
                {
                    user.Diagnostic = diagnostic;
                    user.TestDate = testDate;
                }

                user2.Add(user);
            }

            using (StreamWriter sw = new StreamWriter(path))
            {
                foreach (User user in user2)
                {
                    string json = JsonConvert.SerializeObject(user);
                    sw.WriteLine(json);
                }
            }
        }


        //Funció:Eliminar del fitxer l'usuari indicat.
        //Paràmetres: string amb el nom de l'usuari, string amb la ruta del fitxer.
        public static void DeleteUserData(string userName, string path)
        {
            List<User> user1 = new List<User>();
            List<User> user2 = new List<User>();
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    User user = JsonConvert.DeserializeObject<User>(line);
                    user1.Add(user);
                }
            }

            foreach (User user in user1)
            {
                if (user.Name != userName)
                {
                    user2.Add(user);
                }
            }

            using (StreamWriter sw = new StreamWriter(path))
            {
                foreach (User user in user2)
                {
                    string json = JsonConvert.SerializeObject(user);
                    sw.WriteLine(json);
                }
            }
        }


        //Funció: Comprobar si el fitxer indicat existeix.
        //Paràmetres: string amb la ruta del fitxer.
        //Retorna: true si existeix o flase si no existeix.
        public static bool IfFileExists(string path)
        {
            if (File.Exists(path))
            {
                return true;
            }
            else
            {
                //Console.WriteLine("Error. File doesn't exists on current path.");
            }
            return false;
        }


        //Funció: Funció que obté la data i hora actuals.
        //Retorna: true un string amb la data i hora actuals.
        public static string GetDateTimeNow()
        {
            DateTime now = DateTime.Now;
            string todayDate = now.ToString();
            //Console.WriteLine(todayDate);
            return todayDate;
        }


        public static int SelectCountName(string userName, string path)
        {
            string userName1 = "", diagnostic1 = "", testDate1 = "";
            int counter = 0;

            List<User> user1 = new List<User>();
            List<User> user2 = new List<User>();
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    User user = JsonConvert.DeserializeObject<User>(line);
                    user1.Add(user);
                }
            }

            //Guarda les dades de l'usuari seleccionat en variables.
            foreach (User user in user1)
            {
                if (user.Name == userName)
                {
                    counter++;
                }

            }
            return counter;
        }


        public static void DeleteHistory(string userName, string path)
        {
            int counter = 0;
            List<User> user1 = new List<User>();
            List<User> user2 = new List<User>();
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    User user = JsonConvert.DeserializeObject<User>(line);
                    user1.Add(user);
                }
            }

            foreach (User user in user1)
            {
                if (user.Name != userName)
                {
                    user2.Add(user);
                }else if (user.Name == userName)
                {
                    counter++;
                    if (counter != 1)
                    {
                        user2.Add(user);
                    }
                }
            }

            using (StreamWriter sw = new StreamWriter(path))
            {
                foreach (User user in user2)
                {
                    string json = JsonConvert.SerializeObject(user);
                    sw.WriteLine(json);
                }
            }
        }
    }
}
