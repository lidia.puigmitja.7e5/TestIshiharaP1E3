﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestIshiharaCode
{
    class QuestionManager
    {
        public static int Questiond1 = 0;
        public static string ImageName1, IncorrectValue1;

        //Funció:Seleccionar un usuari pel nom i mostrar les seves dades.
        //Paràmetres: string amb el nom de l'usuari, string amb la ruta del fitxer.
        public static void SelectQuestion(int QuestionId, string pathQuestion/*, string pathAnswer*/)
        {
            //string imageName = "", incorrectValue = "";
            //int questionId1 = 0;

            List<Question> question1 = new List<Question>();
            //List<Question> question2 = new List<Question>();
            using (StreamReader jsonStream = File.OpenText(pathQuestion))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    Question question = JsonConvert.DeserializeObject<Question>(line);
                    question1.Add(question);
                }
            }

            //Guarda les dades de l'usuari seleccionat en variables.
            foreach (Question question in question1)
            {
                if (question.Id == QuestionId)
                {
                    Questiond1 = question.Id;
                    ImageName1 = question.Image;
                    IncorrectValue1 = question.CorrectValue;
                    //question2.Add(question);
                }

            }

            //Mostra per consola les dades de l'usuari seleccionat.
            //Console.WriteLine("Name: " + userName1 + ", Score: " + points1 + ", Daltonisme: " + daltonisme1);

            //Genera un nou fitxer json, amb el mateix nom que l'original, que només inclou l'usuari seleccionat.
            /*using (StreamWriter sw = new StreamWriter(pathAnswer))
            {
                foreach (Question question in question2)
                {
                    string json = JsonConvert.SerializeObject(question);
                    sw.WriteLine(json);
                }
            }*/
        }

        public static int GetQuestionId()
        {
            return Questiond1;
        }

        public static string GetImage1()
        {
            return ImageName1;
        }

        public static string GetIncorrectValue1()
        {
            return IncorrectValue1;
        }
    }
}
