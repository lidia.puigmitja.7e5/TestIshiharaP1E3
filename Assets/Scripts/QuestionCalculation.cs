﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestIshiharaCode
{
    class QuestionCalculation
    {

        public static int CalcularResultats(int id, string nombre, string numero)
        {
            int resultat = 0;
            if (id <= 21)
            {
                if (nombre != numero) resultat += 10;
                else resultat += 0;
            }
            else
            {
                string[] nombreSeparados = nombre.Split("-");
                string[] numeroSeparados = numero.Split("-");
                int posicio = 0;

                if (nombreSeparados[posicio] == numeroSeparados[posicio] && nombreSeparados[posicio + 1] == numeroSeparados[posicio + 1]) resultat += 0;
                else if (nombreSeparados[posicio] == numeroSeparados[posicio] && nombreSeparados[posicio + 1] != numeroSeparados[posicio + 1]) resultat += 10000;
                else if (nombreSeparados[posicio] != numeroSeparados[posicio] && nombreSeparados[posicio + 1] == numeroSeparados[posicio + 1]) resultat += 1000;
                else resultat += 100000;

            }

            return resultat;
        }

        public static string Diagnostic(int resultat)
        {
            string diagnostic;
            if (resultat >= 0 && resultat <= 20) diagnostic = "Bona visió";
            else if (resultat > 20 && resultat <= 80) diagnostic = "Visió lleument afectada, possible daltonisme";
            else if (resultat > 80 && resultat <= 140) diagnostic = "Visió moderadament afectada, possible daltonisme";
            else if (resultat > 140 && resultat <= 200) diagnostic = "Visió severament afectada, possible daltonisme";
            else if (resultat >= 1000 && resultat <= 1080 || resultat >= 2000 && resultat <= 2080 || resultat >= 3000 && resultat <= 3080 || resultat >= 4000 && resultat <= 4080) diagnostic = "Visió lleument afectada, possible daltonisme de tipus Protanopia";
            else if (resultat > 1080 && resultat <= 1140 || resultat > 2080 && resultat <= 2140 || resultat > 3080 && resultat <= 3140 || resultat > 4080 && resultat <= 4140) diagnostic = "Visió moderadament afectada, possible daltonisme de tipus Protanopia";
            else if (resultat > 1140 && resultat <= 1200 || resultat > 2140 && resultat <= 2200 || resultat > 3140 && resultat <= 3200 || resultat > 4140 && resultat <= 4200) diagnostic = "Visió severament afectada, possible daltonisme de tipus Protanopia";
            else if (resultat >= 10000 && resultat <= 10080 || resultat >= 20000 && resultat <= 20080 || resultat >= 30000 && resultat <= 30080 || resultat >= 40000 && resultat <= 40080) diagnostic = "Visió lleument afectada, possible daltonisme de tipus Deuteranopia";
            else if (resultat > 10080 && resultat <= 10140 || resultat > 20080 && resultat <= 20140 || resultat > 30080 && resultat <= 30140 || resultat > 40080 && resultat <= 40140) diagnostic = "Visió moderadament afectada, possible daltonisme de tipus Deuteranopia";
            else if (resultat > 10140 && resultat <= 10200 || resultat > 20140 && resultat <= 20200 || resultat > 30140 && resultat <= 30200 || resultat > 40140 && resultat <= 40200) diagnostic = "Visió severament afectada, possible daltonisme de tipus Deuteranopia";
            else diagnostic = "Visió severament afectada. Impossible diagnosticar tipus";

            return diagnostic;
        }
    }
}
