﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestIshiharaCode
{
    class AnswerSave
    {
        //Funció: Afegir contingut al fitxer json que conte les respostes d'un usuari.
        //Paràmetres: int amb el id de la pregunta, string amb el nom de la imatge, string amb la resposta de l'usuari, string amb la resposta equivocada, string amb la ruta del fitxer.
        public static void AddAnswerToJsonFile(int questionId, string image, string number, string correctValue, string path)
        {
            var answer = new Answer
            {
                Id = questionId,
                Image = image,
                Number = number,
                CorrectValue = correctValue
            };

            string json1 = JsonConvert.SerializeObject(answer);
            File.AppendAllText(path, json1 + "\n");
        }


        public static string SelectUserData(string path)
        {
            int questionId = 0; 
            string image = "", number = "", correctValue = "", diagnostico = "";

            List<Answer> answer1 = new List<Answer>();
            List<Answer> answer2 = new List<Answer>();
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    Answer answer = JsonConvert.DeserializeObject<Answer>(line);
                    answer1.Add(answer);
                }
            }

            //Guarda les dades de l'usuari seleccionat en variables.
            foreach (Answer answer in answer1)
            {
                questionId = answer.Id;
                image = answer.Image;
                number = answer.Number;
                correctValue = answer.CorrectValue;
                diagnostico = QuestionCalculation.Diagnostic(QuestionCalculation.CalcularResultats(questionId, number, correctValue));
                //answer2.Add(answer);
            }

            return diagnostico;
        }


        /*public static int GetQuestionId()
        {
            return Questiond1;
        }

        public static string GetImage1()
        {
            return ImageName1;
        }

        public static string GetIncorrectValue1()
        {
            return IncorrectValue1;
        }*/
    }
}
