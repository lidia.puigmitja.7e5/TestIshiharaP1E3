﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace TestIshiharaCode
{
    class Program
    {
        static void Main(string[] args)
        {
            bool fiPrograma = false;
            do
            {
                Console.WriteLine("Exercicis d'exemple: ");
                Console.WriteLine("1. Mostrar Dades");
                Console.WriteLine("2. Escriure Dades");
                Console.WriteLine("3. Afegir Dades");
                Console.WriteLine("4. Buscar Dades");
                Console.WriteLine("5. Actualitzar Dades");
                Console.WriteLine("6. Eliminar Dades");
                Console.WriteLine("7. Comprobar si arxiu existeix");
                Console.WriteLine("8. Sortir del programa");
                Console.WriteLine("Escogeix una opció: ");
                int opcio = Convert.ToInt32(Console.ReadLine());
                switch (opcio)
                {
                    case 1:
                        string userName;
                        //ResultSave.PrintJsonByConsole(@"..\..\..\testResults.json");
                        //TestStart("Javier");
                        do
                        {
                            Console.WriteLine("Introdueix el teu nom: ");
                             userName = Console.ReadLine();
                        } while (userName.Length < 3);
                        TestStart(userName);
                        break;
                    case 2:

                        //ResultSave.CreateFiledJsonFile("Genaro", "Deuteranopía", "16/10/2022 15:13:26", @"..\..\..\testPoints4.json");
                        //ResultSave.CreateJsonFile(@"..\..\..\testResults.json");
                        userName = "Carlos";
                        string historyPath = @"..\..\..\testResults.json";
                        TestHistory(userName, historyPath);
                        
                        //ShowHistory.GetUserName();
                        /*string diagnostic = ShowHistory.Diagnostic;
                        string testDate = ShowHistory.TestDate;*/
                        //string diagnostic = ShowHistory.GetDiagnostic();
                        //string testDate = ShowHistory.GetTestDate();

                        //Console.WriteLine(diagnostic + " " + testDate);
                        break;
                    case 3:
                        ResultSave.AddTextToJsonFile("Manolo", "Tritanopia", "17/10/2022 15:13:26", @"..\..\..\testResults.json");
                        break;
                    case 4:
                        ResultSave.SelectUserData("Genaro", @"..\..\..\testResults.json");
                        break;
                    case 5:
                        ResultSave.UpdateUserData("Genaro", "Protanopia", "18/10/2022 23:42:53", @"..\..\..\testResults.json");
                        break;
                    case 6:
                        ResultSave.DeleteUserData("Genaro", @"..\..\..\testResults.json");
                        break;
                    case 7:
                        bool exists = ResultSave.IfFileExists(@"..\..\..\testResults.json");
                        Console.WriteLine(exists);
                        break;
                    case 8:
                        Console.WriteLine("Sortint...");
                        fiPrograma = true;
                        break;
                    default:
                        Console.WriteLine("Error. Wrong option.");
                        break;
                }
            } while (fiPrograma == false);
        }

        public static void TestStart(string userName)
        {
            string userFilePath = @"JSON\" + userName + ".json";
            string pathQuestion = @"JSON\TestQuestions.json", image = "", CorrectValue = "", answer1, answer2, answer = "", next;
            string resultSavePath = @"JSON\testResults.json";
            int questionId = 0, posicioArray, finalResult=0, nQuestion;
            string[] array1 = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Nada"};
            string[] array2 = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "Nada"};
            string diagnostico, nQuestion2;

            /*bool filexists = ResultSave.IfFileExists(userFilePath);
            if (filexists == false)
                ResultSave.CreateJsonFile(userFilePath);*/

            ResultSave.CreateJsonFile(userFilePath);

            for (int i = 0; i < 3; i++)
            {
                nQuestion = i + 1;
                nQuestion2 = Convert.ToString(nQuestion);
                Console.WriteLine("Pregunta" + nQuestion + ": ");
                QuestionManager.SelectQuestion(i+1, pathQuestion);
                questionId = QuestionManager.GetQuestionId();
                image = QuestionManager.GetImage1();
                CorrectValue = QuestionManager.GetIncorrectValue1();

                Console.WriteLine(image);
                Console.WriteLine("Quin nombre veus a dins del cercle?");
                switch (nQuestion)
                {
                    case 1:
                    case 4:
                    case 5:
                    case 8:
                    case 9:
                    case 12:
                    case 13:
                    case 16:
                    case 17: 
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                        Console.WriteLine("Escogeix una posició del primer array: ");
                        posicioArray = Convert.ToInt32(Console.ReadLine());
                        answer1 = array1[posicioArray];
                        Console.WriteLine("Escogeix una posició del segon array: ");
                        posicioArray = Convert.ToInt32(Console.ReadLine());
                        answer2 = array2[posicioArray];
                        answer = answer1 + "-" + answer2;
                        break;
                    case 2:
                    case 3:
                    case 6:
                    case 7:
                    case 10:
                    case 11:
                    case 14:
                    case 15:
                    case 18:
                    case 19:
                        Console.WriteLine("Escogeix una posició de l'array: ");
                        posicioArray = Convert.ToInt32(Console.ReadLine());
                        answer = array1[posicioArray];
                        break;
                }

                do
                {
                    Console.WriteLine("Introdueix next: ");
                    next = Console.ReadLine();
                    if (next != "next")
                    {
                        Console.WriteLine("Error, introdueix next: ");
                    }
                    else
                    {
                        AnswerSave.AddAnswerToJsonFile(questionId, image, answer, CorrectValue, userFilePath);
                    }
                } while (next != "next");

            }

            diagnostico = AnswerSave.SelectUserData(userFilePath);
            /*do
            {
                finalResult += QuestionCalculation.CalcularResultats(questionId, CorrectValue, answer);
                
            } while (questionId <= 25);
            diagnostico = QuestionCalculation.Diagnostic(finalResult);*/
            
            //diagnostico = "hola";

            bool filexists1 = ResultSave.IfFileExists(resultSavePath);
            if (filexists1 == false)
                ResultSave.CreateJsonFile(resultSavePath);
            

            int nameCounter = ResultSave.SelectCountName(userName, resultSavePath);
            if (nameCounter > 4)
            {
                ResultSave.DeleteHistory(userName, resultSavePath);
            }

            string testDate = ResultSave.GetDateTimeNow();
            ResultSave.AddTextToJsonFile(userName, diagnostico, testDate, resultSavePath);

            File.Delete(userFilePath);
        }


        public static void TestHistory(string userName, string historyPath)
        {
            string[] historial = ShowHistory.SelectHistory(userName, historyPath);
            Console.WriteLine("Historial de : " + userName);
            for (int i = 0; i < historial.Length; i++)
            {
                Console.WriteLine(historial[i] + ",");
            }
        }

        /*public static void CreateNeededFiles(string userFilePath, string resultSavePath)
        {
            bool filexists = ResultSave.IfFileExists(userFilePath);
            if (filexists == false)
                ResultSave.CreateJsonFile(userFilePath);

            bool filexists1 = ResultSave.IfFileExists(resultSavePath);
            if (filexists1 == false)
                ResultSave.CreateJsonFile(resultSavePath);
        }*/


    }
}
