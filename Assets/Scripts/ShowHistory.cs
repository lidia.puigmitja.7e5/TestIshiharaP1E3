﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace TestIshiharaCode
{
    class ShowHistory
    {
        public static string[] SelectHistory(string userName, string path)
        {
            string diagnostic1 = "", testDate1 = "";
            string[] userHistory = new string[] { "", "", "", "", "", "", "", "", "", ""};
            int posicioArray = 0;

            List<User> user1 = new List<User>();
            List<User> user2 = new List<User>();
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    User user = JsonConvert.DeserializeObject<User>(line);
                    user1.Add(user);
                }
            }

            //Guarda les dades de l'usuari seleccionat en variables.
            foreach (User user in user1)
            {
                if (user.Name == userName)
                {
                    testDate1 = user.TestDate;
                    userHistory[posicioArray] = testDate1;
                    posicioArray++;
                    diagnostic1 = user.Diagnostic;
                    userHistory[posicioArray] = diagnostic1;
                    posicioArray++;

                }

            }

            return userHistory;
        }

        //private static string UserName, Diagnostic, TestDate;

        //Funció:Seleccionar l'historial de resultats d'un usuari.
        //Paràmetres: string amb el nom de l'usuari, string amb la ruta del fitxer.
        /*public static void SelecHistoryData(string userName, string path)
        {
            //string userName1 = "", diagnostic1 = "", testDate1 = "";

            List<User> user1 = new List<User>();
            List<User> user2 = new List<User>();
            using (StreamReader jsonStream = File.OpenText(path))
            {
                string line;
                while ((line = jsonStream.ReadLine()) != null)
                {
                    User user = JsonConvert.DeserializeObject<User>(line);
                    user1.Add(user);
                }
            }

            //Guarda les dades de l'usuari seleccionat en variables.
            foreach (User user in user1)
            {
                if (user.Name == userName)
                {
                    UserName = user.Name;
                    Diagnostic = user.Diagnostic;
                    TestDate = user.TestDate;
                }
            }
        }*/


        /*public static string GetUserName()
        {
            return UserName;
        }

        public static string GetDiagnostic()
        {
            return Diagnostic;
        }

        public static string GetTestDate()
        {
            return TestDate;
        }*/
    }
}
