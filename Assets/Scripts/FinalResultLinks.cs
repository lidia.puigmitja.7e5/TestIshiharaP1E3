using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FinalResultLinks : MonoBehaviour
{
    private GameObject _diagnostico;
    // Start is called before the first frame update
    void Start()
    {
        _diagnostico = GameObject.Find("Diagnostico");
        ResultadoFinal(_diagnostico);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EnlacesBotton(string enlace)
    {
        Application.OpenURL(enlace);
    }

    //Poner texto Resultado en size 22

    public void ResultadoFinal(GameObject diagnostico)
    {
        string texto;
        switch (diagnostico.GetComponent<TMP_Text>().text)
        {
            case "Bona visi�":
                texto = "Felicidades! �Tienes buena visi�n!\n" +
                     "Como informaci�n extra: el daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                     "Aunque el t�rmino daltonismo se utiliza muy com�nmente, el verdadero daltonismo que limita la visi�n del usuario a los tonos blancos y negros es muy raro de ver." +
                     "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul.";
                break;

            case "Visi� lleument afectada, possible daltonisme":
                texto = "�Cuidado! �Visi�n levemente afectada, posible daltonismo! Se aconseja ir al oftalm�logo.\n" +
                     "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                     "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                     "La causa m�s com�n del daltonismo suele deberse a factores hereditarios. Esta afecci�n es m�s frecuente de verse reflejada en las personas con el cromosoma XY, que son m�s propensas a nacer con daltonismo. En los casos heredados existen diferentes grados, suele afectar a ambos ojos y el grado no suele cambiar a lo largo de la vida (ni a mejor, ni a peor)." +
                     "El origen del daltonismo aunque es m�s minoritario tambi�n puede residir en otras posibles causas, que son:" +
                     "Enfermedades como glaucoma, Parkinson, leucemia, entre otros. En estos casos un ojo puede verse m�s afectado que el otro, y es posible que la deficiencia pueda mejorar si se trata la enfermedad base." +
                     " El consumo de algunos medicamentos que tratan enfermedades autoinmunitarias, problemas card�acos, trastornos nerviosos, entre otros." +
                     " El deterioro de la visi�n como consecuencia directa del envejecimiento." +
                     " La exposici�n a ciertas sustancias qu�micas, como disulfuro de carbono o los fertilizantes.";
                break;

            case "Visi� moderadament afectada, possible daltonisme":
                texto = "�Cuidado! �Visi�n moderadamente afectada, posible daltonismo! Se aconseja ir al oftalm�logo.\n" +
                   "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                     "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                     "La causa m�s com�n del daltonismo suele deberse a factores hereditarios. Esta afecci�n es m�s frecuente de verse reflejada en las personas con el cromosoma XY, que son m�s propensas a nacer con daltonismo. En los casos heredados existen diferentes grados, suele afectar a ambos ojos y el grado no suele cambiar a lo largo de la vida (ni a mejor, ni a peor)." +
                     "El origen del daltonismo aunque es m�s minoritario tambi�n puede residir en otras posibles causas, que son:" +
                     "Enfermedades como glaucoma, Parkinson, leucemia, entre otros. En estos casos un ojo puede verse m�s afectado que el otro, y es posible que la deficiencia pueda mejorar si se trata la enfermedad base." +
                     " El consumo de algunos medicamentos que tratan enfermedades autoinmunitarias, problemas card�acos, trastornos nerviosos, entre otros." +
                     " El deterioro de la visi�n como consecuencia directa del envejecimiento." +
                     " La exposici�n a ciertas sustancias qu�micas, como disulfuro de carbono o los fertilizantes.";
                break;

            case "Visi� severament afectada, possible daltonisme":
                texto = "�Cuidado! �Visi�n severamente afectada, posible daltonismo! Se aconseja ir al oftalm�logo.\n" +
                   "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                     "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                     "La causa m�s com�n del daltonismo suele deberse a factores hereditarios. Esta afecci�n es m�s frecuente de verse reflejada en las personas con el cromosoma XY, que son m�s propensas a nacer con daltonismo. En los casos heredados existen diferentes grados, suele afectar a ambos ojos y el grado no suele cambiar a lo largo de la vida (ni a mejor, ni a peor)." +
                     "El origen del daltonismo aunque es m�s minoritario tambi�n puede residir en otras posibles causas, que son:" +
                     "Enfermedades como glaucoma, Parkinson, leucemia, entre otros. En estos casos un ojo puede verse m�s afectado que el otro, y es posible que la deficiencia pueda mejorar si se trata la enfermedad base." +
                     " El consumo de algunos medicamentos que tratan enfermedades autoinmunitarias, problemas card�acos, trastornos nerviosos, entre otros." +
                     " El deterioro de la visi�n como consecuencia directa del envejecimiento." +
                     " La exposici�n a ciertas sustancias qu�micas, como disulfuro de carbono o los fertilizantes.";
                break;

            case "Visi� lleument afectada, possible daltonisme de tipus Protanopia":
                texto = "�Cuidado! Visi�n levemente afectada, posible daltonismo Dicrom�tico de tipo Protanopia! Se aconseja ir al oftalm�logo.\n" +
                      "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                      "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                      "Se trata de un defecto moderadamente grave de origen hereditario, que provoca una disfunci�n en uno de los tres mecanismos b�sicos de color, en cuyo caso la protanopsia o protanopia consiste en la falta total de los fotorreceptores para el color rojo de la retina del ojo."; 
                break;

            case "Visi� moderadament afectada, possible daltonisme de tipus Protanopia":
                texto = "�Cuidado! Visi�n moderadamente afectada, posible daltonismo Dicrom�tico de tipo Protanopia! Se aconseja ir al oftalm�logo.\n" +
                      "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                      "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                      "Se trata de un defecto moderadamente grave de origen hereditario, que provoca una disfunci�n en uno de los tres mecanismos b�sicos de color, en cuyo caso la protanopsia o protanopia consiste en la falta total de los fotorreceptores para el color rojo de la retina del ojo.";
                break;

            case "Visi� severament afectada, possible daltonisme de tipus Protanopia":
                texto = "�Cuidado! Visi�n severamente afectada, posible daltonismo Dicrom�tico de tipo Protanopia! Se aconseja ir al oftalm�logo.\n" +
                      "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                      "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                      "Se trata de un defecto moderadamente grave de origen hereditario, que provoca una disfunci�n en uno de los tres mecanismos b�sicos de color, en cuyo caso la protanopsia o protanopia consiste en la falta total de los fotorreceptores para el color rojo de la retina del ojo.";

                break;

            case "Visi� lleument afectada, possible daltonisme de tipus Deuteranopia":
                texto = "�Cuidado! Visi�n levemente afectada, posible daltonismo Dicrom�tico de tipo Deuteranopia! Se aconseja ir al oftalm�logo.\n" +
                    "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                    "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                    "Se trata de un defecto moderadamente grave de origen hereditario, que provoca una disfunci�n en uno de los tres mecanismos b�sicos de color, en este caso, la deuteranopia o deuteranopia, es la ceguera al color verde y se debe a la carencia de los fotorreceptores de la retina del color verde."; break;

            case "Visi� moderadament afectada, possible daltonisme de tipus Deuteranopia":
                texto = "�Cuidado! Visi�n moderadamente afectada, posible daltonismo Dicrom�tico de tipo Deuteranopia! Se aconseja ir al oftalm�logo.\n" +
                    "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                    "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                    "Se trata de un defecto moderadamente grave de origen hereditario, que provoca una disfunci�n en uno de los tres mecanismos b�sicos de color, en este caso, la deuteranopia o deuteranopia, es la ceguera al color verde y se debe a la carencia de los fotorreceptores de la retina del color verde.";
                break;

            case "Visi� severament afectada, possible daltonisme de tipus Deuteranopia":
                texto = "�Cuidado! Visi�n severamente afectada, posible daltonismo Dicrom�tico de tipo Deuteranopia! Se aconseja ir al oftalm�logo.\n" +
                      "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                      "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                      "Se trata de un defecto moderadamente grave de origen hereditario, que provoca una disfunci�n en uno de los tres mecanismos b�sicos de color, en este caso, la deuteranopia o deuteranopia, es la ceguera al color verde y se debe a la carencia de los fotorreceptores de la retina del color verde.";
                break;

            case "Visi� severament afectada. Impossible diagnosticar tipus":
                texto = "�Cuidado! �Visi�n severamente afectada,  Impossible diagnosticar el tipo de daltonismo!!Se aconseja ir al oftalm�logo.\n" +
                   "El daltonismo, m�s espec�ficamente conocido como deficiente percepci�n del color, se caracteriza principalmente por la incapacidad para percibir la diferencia entre ciertos colores por parte de aquellas personas que la padecen." +
                     "La mayor parte de gente que sufre daltonismo presenta dificultades a la hora de distinguir entre ciertos tonos de verde y rojo, o incluso en algunos casos tambi�n entre amarillo y azul." +
                     "La causa m�s com�n del daltonismo suele deberse a factores hereditarios. Esta afecci�n es m�s frecuente de verse reflejada en las personas con el cromosoma XY, que son m�s propensas a nacer con daltonismo. En los casos heredados existen diferentes grados, suele afectar a ambos ojos y el grado no suele cambiar a lo largo de la vida (ni a mejor, ni a peor)." +
                     "El origen del daltonismo aunque es m�s minoritario tambi�n puede residir en otras posibles causas, que son:" +
                     "Enfermedades como glaucoma, Parkinson, leucemia, entre otros. En estos casos un ojo puede verse m�s afectado que el otro, y es posible que la deficiencia pueda mejorar si se trata la enfermedad base." +
                     " El consumo de algunos medicamentos que tratan enfermedades autoinmunitarias, problemas card�acos, trastornos nerviosos, entre otros." +
                     " El deterioro de la visi�n como consecuencia directa del envejecimiento." +
                     " La exposici�n a ciertas sustancias qu�micas, como disulfuro de carbono o los fertilizantes.";
                break;

            default:
                texto = "Hello World";
                break;

                



        }
        GameObject.Find("Resultado").GetComponent<TMP_Text>().text = texto;
    }

}
