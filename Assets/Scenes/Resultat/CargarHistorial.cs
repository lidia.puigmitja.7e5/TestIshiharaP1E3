using TMPro;
using UnityEngine;
using TestIshiharaCode;

public class CargarHistorial : MonoBehaviour
{
   // Start is called before the first frame update
    void Start()
    {
        string path = Application.dataPath + "/StreamingAssets/testResults.json";
        string[] historial = ShowHistory.SelectHistory(GameObject.Find("PlayerName").GetComponent<TMP_Text>().text, path);
        GameObject[] filas = {
            GameObject.Find("Fecha1"), GameObject.Find("Result1"),
            GameObject.Find("Fecha2"), GameObject.Find("Result2"),
            GameObject.Find("Fecha3"), GameObject.Find("Result3"),
            GameObject.Find("Fecha4"), GameObject.Find("Result4"),
            GameObject.Find("Fecha5"), GameObject.Find("Result5")
        };
        for (int i = 0; i < historial.Length; i++)
        {
            filas[i].GetComponent<TMP_Text>().text = historial[i];
        }
    }
        
        
    

    // Update is called once per frame
    void Update()
    {
        
    }
  
   

   

}



