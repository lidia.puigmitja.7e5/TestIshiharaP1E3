using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SetResult : MonoBehaviour
{

    public GameObject Fecha;
    public GameObject Resultado;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetRetResult(string fecha, string resultado)
    {
        Fecha.GetComponent<TextMeshPro>().text = fecha;
        Resultado.GetComponent<TextMeshPro>().text = resultado;
    }
}
